FROM python:3.7

ADD requirements.txt /tmp/requirements.txt

RUN pip install --upgrade pip && pip install -r /tmp/requirements.txt --no-cache-dir

ADD . /work

WORKDIR /work
