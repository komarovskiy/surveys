build:
	docker-compose up --build

run:
	docker-compose up -d

stop:
	docker-compose stop

restart:
	docker-compose restart -d

shell:
	docker-compose exec django python manage.py shell_plus

migrations:
	docker-compose exec django python manage.py makemigrations $(app)

migrate:
	docker-compose exec django python manage.py migrate

su:
	docker-compose exec django python manage.py createsuperuser

test:
	docker-compose exec django python manage.py test

bootstrap: run migrate su