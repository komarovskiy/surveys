from django.views.generic import ListView, DetailView, View, CreateView
from django.shortcuts import redirect
from django.urls import reverse


from surveys.models import Survey, UserSurveySession, UserSurveySessionAnswer
from surveys.forms import UserSurveySessionAnswerForm, UserSurveySessionForm


class HomePageView(ListView):
    model = Survey
    template_name = 'index.html'
    context_object_name = 'surveys'

    def get_queryset(self):
        return self.model.objects.filter(questions__isnull=False).distinct()

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['title'] = 'Surveys'
        return context


class SurveyDetailView(DetailView):
    model = Survey
    template_name = 'survey.html'
    context_object_name = 'survey'

    def get_context_data(self, **kwargs):
        context = super(SurveyDetailView, self).get_context_data(**kwargs)
        context['title'] = self.object.name
        context['form'] = UserSurveySessionForm
        return context


class JoinSurveyView(CreateView):
    model = UserSurveySession
    template_name = 'survey.html'
    form_class = UserSurveySessionForm

    def dispatch(self, request, *args, **kwargs):
        if not request.session.session_key:
            request.session.create()
        try:
            uss = UserSurveySession.objects.get(session=request.session.session_key)
            if len(uss.questions_left) == 0:
                return redirect(reverse('survey_result', kwargs={'slug': self.kwargs['slug']}))
            else:
                first_question = int(uss.questions_left.split(",")[0].strip())
                return redirect(reverse('question', kwargs={'slug': self.kwargs['slug'], 'pk': first_question}))
        except UserSurveySession.DoesNotExist:
            pass
        return super(JoinSurveyView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        first_question = int(self.object.questions_left.split(",")[0].strip())
        return reverse('question', kwargs={'slug': self.kwargs['slug'], 'pk': first_question})

    def form_valid(self, form):
        self.object = form.save(commit=False)
        survey = Survey.objects.get(slug=self.kwargs['slug'])
        questions_list = list(survey.questions.order_by('?').values_list('pk', flat=True))
        questions_str_list = [str(q) for q in questions_list]
        questions = ",".join(questions_str_list)
        self.object.session = self.request.session.session_key
        self.object.survey = survey
        self.object.questions_left = questions
        self.object.save()
        return super(JoinSurveyView, self).form_valid(form)


class QuestionFormView(CreateView):
    model = UserSurveySessionAnswer
    form_class = UserSurveySessionAnswerForm
    template_name = 'question.html'

    def dispatch(self, request, *args, **kwargs):
        self.uss = UserSurveySession.objects.get(
            session=request.session.session_key,
            survey__slug=self.kwargs['slug']
        )
        self.survey = Survey.objects.get(slug=self.kwargs['slug'])
        self.question = self.survey.questions.get(pk=self.kwargs['pk'])

        return super(QuestionFormView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(QuestionFormView, self).get_form_kwargs()
        kwargs.update({'question': self.question})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(QuestionFormView, self).get_context_data(**kwargs)
        context['survey'] = self.survey
        context['question'] = self.question
        return context
    
    def form_valid(self, form):
        question = self.uss.survey.questions.get(pk=self.kwargs['pk'])
        correct_answers = list(question.answers.filter(is_correct=True).values_list('pk', flat=True))
        answers = [a.pk for a in form.cleaned_data['answers']]

        self.object = form.save(commit=False)
        self.object.survey_session = self.uss
        self.object.question = question
        self.object.is_correct = sorted(correct_answers) == sorted(answers)
        self.object.save()

        left = self.uss.questions_left.split(",")
        self.first = left.pop(0)
        if self.uss.questions_answered:
            answered = self.uss.questions_answered.split(",")
        else:
            answered = []
        answered.append(self.first)
        if len(left) > 0:
            self.next = left[0]
        else:
            self.next = None
        self.uss.questions_left = ",".join(left)
        self.uss.questions_answered = ",".join(answered)
        self.uss.save()

        return super(QuestionFormView, self).form_valid(form)

    def get_success_url(self):
        if len(self.uss.questions_left) == 0:
            return reverse('survey_result', kwargs={'slug': self.kwargs['slug']})
        else:
            return reverse('question', kwargs={'slug': self.kwargs['slug'], 'pk': int(self.next)})


class ResultView(DetailView):
    template_name = 'results.html'
    model = UserSurveySession
    context_object_name = 'result'

    def get_object(self, queryset=None):
        return self.model.objects.get(
            session=self.request.session.session_key, survey__slug=self.kwargs['slug']
        )

    def get_context_data(self, **kwargs):
        context = super(ResultView, self).get_context_data(**kwargs)
        context['show_results'] = True
        context['title'] = self.object.survey.name
        results_is_correct, results_total = self.object.result_answers()
        result_percent = self.object.result_percentage()
        context['results_percent'] = "{0:.2f}%".format(result_percent)
        context['results_is_correct'] = results_is_correct
        context['results_total'] = results_total
        return context
