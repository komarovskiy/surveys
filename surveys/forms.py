from django import forms


from surveys.models import UserSurveySessionAnswer, UserSurveySession


class UserSurveySessionForm(forms.ModelForm):
    class Meta:
        model = UserSurveySession
        fields = ['name', ]
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter your name',
            })
        }


class UserSurveySessionAnswerForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        question = kwargs.pop('question')
        super(UserSurveySessionAnswerForm, self).__init__(*args, **kwargs)
        self.fields['answers'].queryset = question.answers.all()

    class Meta:
        model = UserSurveySessionAnswer
        fields = ['answers', ]
        widgets = {
            'answers': forms.CheckboxSelectMultiple()
        }
