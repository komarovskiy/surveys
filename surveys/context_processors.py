from surveys.models import UserSurveySession


def passed_tests(request):
    passed_tests = UserSurveySession.objects.filter(
        session=request.session.session_key, questions_left=""
    ).values_list('survey__slug', flat=True)

    return {
        'passed_tests': list(passed_tests),
    }
