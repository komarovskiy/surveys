from django.contrib import admin

from surveys.models import Survey, Question, Answer, UserSurveySession, UserSurveySessionAnswer
from surveys.forms import UserSurveySessionAnswerForm


class SurveyAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


class AnswerInline(admin.TabularInline):
    extra = 0
    model = Answer


class QuestionAdmin(admin.ModelAdmin):
    inlines = [AnswerInline]


class UserAnswerInline(admin.TabularInline):
    extra = 0
    model = UserSurveySessionAnswer

    def get_form(self):
        return UserSurveySessionAnswerForm


class UserSurveySessionAdmin(admin.ModelAdmin):
    list_display = ['survey', 'name', 'created', 'modified', 'result', 'result_percentage', 'status']
    change_form_template = 'admin/result_details.html'

    def result(self, obj):
        return "{} / {}".format(*obj.result_answers())

    def result_percentage(self, obj):
        return "{0:.2f}%".format(obj.result_percentage())

    def status(self, obj):
        return obj.status()


admin.site.register(Survey, SurveyAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(UserSurveySession, UserSurveySessionAdmin)

