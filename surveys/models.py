from django.db import models


from model_utils.models import TimeStampedModel
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill


class Survey(TimeStampedModel):
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=255, unique=True, db_index=True)
    description = models.TextField()
    image_cover = models.ImageField(upload_to='surveys/image_covers')
    image_cover_thumb = ImageSpecField(source='image_cover',
                                       processors=[ResizeToFill(700, 400)],
                                       format='JPEG', options={'quality': 90})
    image_cover_normal = ImageSpecField(source='image_cover',
                                       processors=[ResizeToFill(1140, 600)],
                                       format='JPEG', options={'quality': 90})

    def __str__(self):
        return self.name

    def top_results(self):
        results = self.sessions.filter(questions_left="")
        result = list(sorted(results, key=lambda x: x.result_percentage()))
        return result

    class Meta:
        ordering = ('name',)


class Question(TimeStampedModel):
    survey = models.ForeignKey(Survey, on_delete=models.CASCADE, related_name='questions')
    question = models.TextField()

    def __str__(self):
        return self.question


class Answer(TimeStampedModel):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    answer = models.CharField(max_length=255)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return self.answer


class UserSurveySession(TimeStampedModel):
    session = models.CharField(max_length=255, unique=True)
    survey = models.ForeignKey(Survey, related_name='sessions', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)

    # In case of PostgreSQL use django.contrib.postgres.fields.ArrayField. Less work then :D

    questions_left = models.TextField(blank=True, null=True)
    questions_answered = models.TextField(blank=True, null=True)

    def result_answers(self):
        results_total = self.survey.questions.count()
        results_is_correct = self.user_answers.filter(is_correct=True).count()
        return results_is_correct, results_total

    def result_percentage(self):
        c, t = self.result_answers()
        return c / t * 100

    def status(self):
        if not self.questions_left:
            return "Completed"
        else:
            return "In progress"

    def __str__(self):
        return "{}'s results of {}".format(self.name, self.survey.name)

    class Meta:
        verbose_name = "User Survey"
        verbose_name_plural = "User Surveys"


class UserSurveySessionAnswer(TimeStampedModel):
    survey_session = models.ForeignKey(UserSurveySession, related_name='user_answers', on_delete=models.CASCADE)
    question = models.ForeignKey(Question, related_name='uss_answers', on_delete=models.SET_NULL, null=True)
    answers = models.ManyToManyField(Answer)
    is_correct = models.BooleanField(default=False)
