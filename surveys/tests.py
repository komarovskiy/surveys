import datetime


from django.test import TestCase
from django.utils import timezone
from django.conf import settings



from surveys.models import Survey, Question, Answer, UserSurveySession, UserSurveySessionAnswer


SURVEY_DATA = {
    'name': 'Test Case Survey 1',
    'slug': 'test-case-survey-1',
}

QUESTIONS = [
    {
        'id': 10,
        'question': "The capital of GB",
        'answers': [
            {
                'id': 1,
                'answer': 'London',
                'is_correct': True
            },
            {
                'id': 2,
                'answer': 'Zurich',
                'is_correct': False
            },
            {
                'id': 3,
                'answer': 'New York',
                'is_correct': False
            },
            {
                'id': 4,
                'answer': 'Rome',
                'is_correct': False
            },
        ]
    },
    {
        'id': 11,
        'question': "Two teams that presents London?",
        'answers': [
            {
                'id': 5,
                'answer': 'Arsenal',
                'is_correct': True
            },
            {
                'id': 6,
                'answer': 'Wolves',
                'is_correct': False
            },
            {
                'id': 7,
                'answer': 'Everton',
                'is_correct': False
            },
            {
                'id': 8,
                'answer': 'Chelsea',
                'is_correct': True
            },
        ]
    },
    {
        'id': 12,
        'question': "FIFA World Champion 1998?",
        'answers': [
            {
                'id': 9,
                'answer': 'Netherlands',
                'is_correct': False
            },
            {
                'id': 10,
                'answer': 'Brazil',
                'is_correct': False
            },
            {
                'id': 11,
                'answer': 'France',
                'is_correct': True
            },
            {
                'id': 12,
                'answer': 'Italy',
                'is_correct': False
            },
        ]
    },
]

SURVEY_SESSION_DATA = {
    'name': 'Tester',
    'session': '01877e65s'
}

USER_ANSWERS = [
    {'question': 10, 'answers': [1]},
    {'question': 11, 'answers': [5,8]},
    {'question': 12, 'answers': [12]}
]


class AppTestCase(TestCase):

    def setUp(self):
        self.survey = Survey.objects.create(**SURVEY_DATA)
        for q in QUESTIONS:
            question = Question.objects.create(survey=self.survey, question=q['question'])
            for a in q['answers']:
                Answer.objects.create(**a, question=question)
        self.questions = self.survey.questions.all()
        self.survey_session = UserSurveySession.objects.create(
            **SURVEY_SESSION_DATA,
            survey=self.survey,
            questions_left=",".join([str(q) for q in self.survey.questions.order_by('?').values_list('pk', flat=True)])
        )

    def test_survey_created_with_correct_name(self):
        self.assertEqual(self.survey.name, SURVEY_DATA['name'])
        self.assertEqual(self.survey.slug, SURVEY_DATA['slug'])

    def test_survey_questions_quantity(self):
        self.assertEqual(self.survey.questions.count(), len(QUESTIONS))

    def test_survey_session_has_same_questions_as_survey(self):
        survey_session_questions_list = self.survey_session.questions_left.split(",")
        for question in self.questions:
            self.assertTrue(str(question.pk) in survey_session_questions_list)

    def test_user_answers(self):
        for user_answers in USER_ANSWERS:
            ussa = UserSurveySessionAnswer()
            question = self.questions.get(pk=user_answers['question'])
            answers = Answer.objects.filter(pk__in=user_answers['answers']).values_list('pk', flat=True)
            ussa.question = question
            ussa.survey_session = self.survey_session
            ussa.save()
            ussa.is_correct = sorted(user_answers['answers']) == sorted(
                question.answers.filter(is_correct=True).values_list('pk', flat=True))
            for a in answers:
                ussa.answers.add(a)
            ussa.save()

        correct, total = self.survey_session.result_answers()
        self.assertEqual(correct, 2)
        self.assertEqual(total, 3)

